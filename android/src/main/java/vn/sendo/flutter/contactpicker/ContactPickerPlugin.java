// Copyright 2017 Michael Goderbauer. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

package vn.sendo.flutter.contactpicker;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Resources;
import android.database.Cursor;
import android.net.Uri;
import android.provider.ContactsContract;
import android.provider.Settings;
import android.util.Log;
import android.widget.ListView;

import java.util.ArrayList;
import java.util.HashMap;

import io.flutter.plugin.common.MethodChannel;
import io.flutter.plugin.common.MethodChannel.MethodCallHandler;
import io.flutter.plugin.common.MethodChannel.Result;
import io.flutter.plugin.common.MethodCall;
import io.flutter.plugin.common.PluginRegistry;
import io.flutter.plugin.common.PluginRegistry.Registrar;

import static android.app.Activity.RESULT_OK;

public class ContactPickerPlugin implements MethodCallHandler, PluginRegistry.ActivityResultListener {
    public static void registerWith(Registrar registrar) {
        final MethodChannel channel = new MethodChannel(registrar.messenger(), "native_contact_picker");
        ContactPickerPlugin instance = new ContactPickerPlugin(registrar.activity());
        registrar.addActivityResultListener(instance);
        channel.setMethodCallHandler(instance);
    }

    private ContactPickerPlugin(Activity activity) {
        this.activity = activity;
    }

    private static int PICK_CONTACT = 2015;

    private Activity activity;
    private Result pendingResult;

    @Override
    public void onMethodCall(MethodCall call, Result result) {
        if (call.method.equals("selectContact")) {
            if (pendingResult != null) {
                pendingResult.error("multiple_requests", "Cancelled by a second request.", null);
                pendingResult = null;
            }
            pendingResult = result;

            Intent i = new Intent(Intent.ACTION_PICK, ContactsContract.Contacts.CONTENT_URI);
            activity.startActivityForResult(i, PICK_CONTACT);
        } else if (call.method.equals("openSettings")) {
            openSettings();
            pendingResult.success(true);
            pendingResult = null;
        }
        else {
            result.notImplemented();
        }
    }
    // private Registrar registrar;

    private void openSettings() {
        //Activity activity = registrar.activity();
        Intent intent = new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS, Uri.parse("package:" + activity.getPackageName()));
        intent.addCategory(Intent.CATEGORY_DEFAULT);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        activity.startActivity(intent);
    }

    @Override
    public boolean onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode != PICK_CONTACT) {
            return false;
        }
        if (resultCode != RESULT_OK) {
            pendingResult.success(null);
            pendingResult = null;
            return true;
        }

        Uri contactUri = data.getData();
        Cursor cursor = activity.getContentResolver().query(contactUri, null, null, null, null);
        cursor.moveToFirst();

        final ArrayList<String> values = new ArrayList<String>();
        String fullName= null;
        try {

            // column index of the contact ID
            String id = cursor.getString(cursor.getColumnIndex(ContactsContract.Contacts._ID));
            // column index of the contact name
            fullName  = cursor.getString(cursor.getColumnIndex(ContactsContract.Contacts.DISPLAY_NAME));
            // column index of the phone number
            Cursor pCur = activity.getContentResolver().query(ContactsContract.CommonDataKinds.Phone.CONTENT_URI,null,
                    ContactsContract.CommonDataKinds.Phone.CONTACT_ID +" = ?",
                    new String[]{id}, null);
            while (pCur.moveToNext()) {
                values.add(pCur.getString(
                        pCur.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER)));
            }
            pCur.close();
            // column index of the email
            Cursor emailCur = activity.getContentResolver().query(
                    ContactsContract.CommonDataKinds.Email.CONTENT_URI,
                    null,
                    ContactsContract.CommonDataKinds.Email.CONTACT_ID + " = ?",
                    new String[]{id}, null);
            while (emailCur.moveToNext()) {
                // This would allow you get several email addresses
                // if the email addresses were stored in an array
                values.add( emailCur.getString(
                        emailCur.getColumnIndex(ContactsContract.CommonDataKinds.Email.ADDRESS)));
            }
            emailCur.close();
        } catch (Exception e) {
            e.printStackTrace();
        }

        final HashMap<String, Object> contact = new HashMap<>();
        contact.put("fullName", fullName);

        if(values.size() > 1) {
            AlertDialog alertDialog = new AlertDialog.Builder(activity, android.R.style.Theme_Material_Dialog_Alert)
                    .setSingleChoiceItems(values.toArray(new String[values.size()]), -1, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            ListView lv = ((AlertDialog) dialogInterface).getListView();
                            lv.setTag(new Integer(i));
                        }

                    }).setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, final int i) {
                            ListView lv = ((AlertDialog) dialogInterface).getListView();
                            Integer selected = (Integer) lv.getTag();
                            if (selected != null) {
                                contact.put("phoneNumber", values.get(selected));
                                pendingResult.success(contact);
                                pendingResult = null;
                            }

                            dialogInterface.dismiss();

                        }
                    }).create();
            alertDialog.show();
        } else {
            contact.put("phoneNumber", values.get(0));
            pendingResult.success(contact);
            pendingResult = null;
        }
        return true;

    }
}
